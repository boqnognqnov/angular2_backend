<?php
/**
 * Created by Docebo Spa.
 *
 * @link https://www.docebo.com/
 * @copyright Copyright (c) 2016 Docebo Spa
 */

namespace App\Models;

use PDO;


class AnimalTypeModel extends Model
{

    public static function getNameOfKindId($kindId)
    {
        $sql = 'SELECT at.kind FROM animal_type AS at WHERE at.id = :kindId';
        $stmt = self::$dbh->prepare($sql);
        $stmt->execute([':kindId' => $kindId]);
        $result = $stmt->fetchAll(PDO::FETCH_COLUMN);
        if ($result) {
            $result = $result[0];
        }
        return $result;
        
    }

}