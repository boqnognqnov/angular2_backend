<?php
/**
 * Created by Docebo Spa.
 *
 * @link https://www.docebo.com/
 * @copyright Copyright (c) 2016 Docebo Spa
 */

namespace App\Models;

class Model
{

    protected static $dbh;

    public static function setDbhConnection($connection)
    {
        self::$dbh = $connection;
    }

    public static function getDbhConnection()
    {
        return self::$dbh;
    }


}