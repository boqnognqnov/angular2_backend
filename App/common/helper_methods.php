<?php
function dump($data)
{
    $file = 'dump.log';

    $current = file_get_contents($file);

    if (!empty($data)) {
        if (!is_array($data)) {
            $current .= $data . "\n";
        } else {
            $current .= print_r($data, 1) . "\n";
        }

    } else {
        $current .= 'empty' . "\n";
    }

    file_put_contents($file, $current);
}