<?php
/**
 * Created by Docebo Spa.
 *
 * @link https://www.docebo.com/
 * @copyright Copyright (c) 2016 Docebo Spa
 */

namespace App\Controllers;

use App\Models\Model;
use bootstrap\cache\Request;


class Controller
{
    public static $dbh;


    public function __construct()
    {
        self::$dbh = Model::getDbhConnection();
     

    }

}