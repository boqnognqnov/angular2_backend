<?php
/**
 * Created by Docebo Spa.
 *
 * @link https://www.docebo.com/
 * @copyright Copyright (c) 2016 Docebo Spa
 */
//
namespace App\Controllers;

use App\Models\AnimalTypeModel;
use bootstrap\cache\Request;
use PDO;
use PDOException;


class AnimalsController extends Controller
{

    public function index($kind)
    {

        $sql = "SELECT a.id,a.name,a.image,a.description,animal_type.kind
                FROM animal AS a 
                INNER JOIN animal_type ON a.type_id=animal_type.id
                WHERE animal_type.kind LIKE :kind";

        $stmt = self::$dbh->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $stmt->execute(array(':kind' => $kind));
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        header('Access-Control-Allow-Headers: Content-Type, x-xsrf-token');
        echo json_encode($result);

    }

    public function view($animalId)
    {
        $sql = "SELECT a.id,a.name,a.image,a.description,animal_type.kind as kind_name,animal_type.id as kind_id
                FROM animal AS a 
                INNER JOIN animal_type ON a.type_id=animal_type.id
                WHERE a.id = :animalId";

        $stmt = self::$dbh->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $stmt->execute(array(':animalId' => $animalId));
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($result) {
            header('Access-Control-Allow-Headers: Content-Type, x-xsrf-token');
            echo json_encode($result[0]);
        }
    }


    public function addEditAnimal()
    {
        $request = Request::get('add_edit_animal');


        $result = [];
        dump($request['id']);
        if ($request['id'] == 0) {
            $sql = 'INSERT INTO animal(type_id,name,image,description) VALUES (:type_id,:name,:image,:description)';
        } else {
            $sql = 'UPDATE animal SET type_id= :type_id, name= :name, image= :image, description= :description WHERE id=' . $request['id'];
        }

        try {
            $stmt = self::$dbh->prepare($sql);
            $stmt->execute(array(':type_id' => $request['kind_id'], ':name' => $request['name'], ':image' => $request['image'], ':description' => $request['description']));

            if ($request['id'] == 0) {
                $request['id'] = self::$dbh->lastInsertId();
            }

            $staus = 'success';
            $result['data'] = $request;
            $result['data']['kind_id'] = $request['kind_id'];
            $result['data']['kind_id'] = AnimalTypeModel::getNameOfKindId($request['kind_id']);

        } catch (PDOException $e) {
            dump($e->getMessage());
            $staus = 'error';
        }
        $result['status'] = $staus;

        header('Access-Control-Allow-Headers: Content-Type, x-xsrf-token');
        echo json_encode($result);
    }


    public function getKinds()
    {

        $sql = "SELECT at.id, at.kind AS kind_name FROM animal_type AS at";

        $stmt = self::$dbh->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        header('Access-Control-Allow-Headers: Content-Type, x-xsrf-token');
        echo json_encode($result);

    }


    public function deleteAnimal($animalId)
    {
        try {

            $staus['status'] = 'success';

            $sql = "DELETE FROM animal WHERE id= :animalId";
            $stmt = self::$dbh->prepare($sql);
            $stmt->execute(array(':animalId' => $animalId));

        } catch (PDOException $e) {
            dump($e->getMessage());
            $staus = 'error';
        }
        $result['status'] = $staus;

        header('Access-Control-Allow-Headers: Content-Type, x-xsrf-token');
        echo json_encode($result);
    }


    public function addEditKind()
    {
        $request = Request::get();


        $result = [];
        dump($request['id']);
        if ($request['id'] == 0) {
            $sql = 'INSERT INTO animal_type(kind) VALUES (:kind)';
        } else {
            $sql = 'UPDATE animal_type SET kind= :kind WHERE id=' . $request['id'];
        }

        try {
            $stmt = self::$dbh->prepare($sql);
            $stmt->execute(array(':kind' => $request['kind_name']));

            if ($request['id'] == 0) {
                $request['id'] = self::$dbh->lastInsertId();
            }

            $status = 'success';
            $result['data'] = $request;


        } catch (PDOException $e) {
            dump($e->getMessage());
            $status = 'error';
        }
        $result['status'] = $status;

        header('Access-Control-Allow-Headers: Content-Type, x-xsrf-token');
        echo json_encode($result);
    }


    public function deleteKind($kindId)
    {
        try {

            $staus['status'] = 'success';

            $sql = "DELETE FROM animal WHERE type_id= :kindId";
            $stmt = self::$dbh->prepare($sql);
            $stmt->execute(array(':kindId' => $kindId));


            $sql = "DELETE FROM animal_type WHERE id= :kindId";
            $stmt = self::$dbh->prepare($sql);
            $stmt->execute(array(':kindId' => $kindId));

        } catch (PDOException $e) {
            dump($e->getMessage());
            $staus = 'error';
        }
        $result['status'] = $staus;

        header('Access-Control-Allow-Headers: Content-Type, x-xsrf-token');
        echo json_encode($result);
    }


}