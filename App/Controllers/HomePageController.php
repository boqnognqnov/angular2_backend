<?php
/**
 * Created by Docebo Spa.
 *
 * @link https://www.docebo.com/
 * @copyright Copyright (c) 2016 Docebo Spa
 */
//
namespace App\Controllers;

use PDO;


class HomePageController extends Controller
{

    public function index()
    {
        $sql = "SELECT home_page_items.item FROM home_page_items";


        $stmt = self::$dbh->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_COLUMN);
        header('Access-Control-Allow-Headers: Content-Type, x-xsrf-token');
        echo json_encode($result);


    }
}