<?php
/**
 * Created by Docebo Spa.
 *
 * @link https://www.docebo.com/
 * @copyright Copyright (c) 2016 Docebo Spa
 */

require_once __DIR__ . '/vendor/autoload.php';


class config_helper
{
    public static function loadConfig()
    {

        $connection = self::defineNewPdoConnecton();

        \App\Models\Model::setDbhConnection($connection);
    }

    static function defineNewPdoConnecton()
    {
        $dbh = new PDO('mysql:host='
            . DB_HOST . ';' . 'dbname='
            . DB_NAME,
            DB_USER, DB_PASSWORD,
            array(PDO::ATTR_PERSISTENT => false));

        $dbh->exec("SET CHARACTER SET utf8");

        return $dbh;
    }

}