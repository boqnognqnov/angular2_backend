/*
Navicat MySQL Data Transfer

Source Server         : Hydra
Source Server Version : 50714
Source Host           : localhost:3306
Source Database       : angular_test

Target Server Type    : MYSQL
Target Server Version : 50714
File Encoding         : 65001

Date: 2017-02-05 03:14:59
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for animal
-- ----------------------------
DROP TABLE IF EXISTS `animal`;
CREATE TABLE `animal` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`type_id`  int(11) NOT NULL ,
`name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
`image`  varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
`description`  text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
`created_at`  timestamp NULL DEFAULT NULL ,
`updated_at`  timestamp NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_unicode_ci
AUTO_INCREMENT=60

;

-- ----------------------------
-- Records of animal
-- ----------------------------
BEGIN;
INSERT INTO `animal` VALUES ('59', '114', 'sdfdsf', 'http://writm.com/wp-content/uploads/2016/08/Cat-hd-wallpapers.jpg', 'fsdf', null, null);
COMMIT;

-- ----------------------------
-- Table structure for animal_type
-- ----------------------------
DROP TABLE IF EXISTS `animal_type`;
CREATE TABLE `animal_type` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`kind`  varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_unicode_ci
AUTO_INCREMENT=119

;

-- ----------------------------
-- Records of animal_type
-- ----------------------------
BEGIN;
INSERT INTO `animal_type` VALUES ('114', 'aaaaaa2'), ('118', 'sadasdasdasd');
COMMIT;

-- ----------------------------
-- Table structure for home_page
-- ----------------------------
DROP TABLE IF EXISTS `home_page`;
CREATE TABLE `home_page` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`image`  varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_unicode_ci
AUTO_INCREMENT=4

;

-- ----------------------------
-- Records of home_page
-- ----------------------------
BEGIN;
INSERT INTO `home_page` VALUES ('1', 'asdasdasd'), ('2', 'dasdasda'), ('3', 'aaaaaaaaaa');
COMMIT;

-- ----------------------------
-- Table structure for home_page_items
-- ----------------------------
DROP TABLE IF EXISTS `home_page_items`;
CREATE TABLE `home_page_items` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`item`  varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_unicode_ci
AUTO_INCREMENT=4

;

-- ----------------------------
-- Records of home_page_items
-- ----------------------------
BEGIN;
INSERT INTO `home_page_items` VALUES ('1', 'aaaaa'), ('2', 'bbbb'), ('3', 'ccccc');
COMMIT;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`migration`  varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
`batch`  int(11) NOT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_unicode_ci
AUTO_INCREMENT=9

;

-- ----------------------------
-- Records of migrations
-- ----------------------------
BEGIN;
INSERT INTO `migrations` VALUES ('1', '2014_10_12_000000_create_users_table', '1'), ('2', '2014_10_12_100000_create_password_resets_table', '1'), ('3', '2016_10_27_113755_animal_type', '1'), ('5', '2016_10_27_114335_home_page', '2'), ('7', '2016_10_27_113807_animal', '3'), ('8', '2016_10_27_114452_home_page_items', '3');
COMMIT;

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
`email`  varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
`token`  varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
`created_at`  timestamp NULL DEFAULT NULL ,
INDEX `password_resets_email_index` (`email`) USING BTREE ,
INDEX `password_resets_token_index` (`token`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_unicode_ci

;

-- ----------------------------
-- Records of password_resets
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
`email`  varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
`password`  varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
`remember_token`  varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`created_at`  timestamp NULL DEFAULT NULL ,
`updated_at`  timestamp NULL DEFAULT NULL ,
PRIMARY KEY (`id`),
UNIQUE INDEX `users_email_unique` (`email`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_unicode_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Records of users
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Auto increment value for animal
-- ----------------------------
ALTER TABLE `animal` AUTO_INCREMENT=60;

-- ----------------------------
-- Auto increment value for animal_type
-- ----------------------------
ALTER TABLE `animal_type` AUTO_INCREMENT=119;

-- ----------------------------
-- Auto increment value for home_page
-- ----------------------------
ALTER TABLE `home_page` AUTO_INCREMENT=4;

-- ----------------------------
-- Auto increment value for home_page_items
-- ----------------------------
ALTER TABLE `home_page_items` AUTO_INCREMENT=4;

-- ----------------------------
-- Auto increment value for migrations
-- ----------------------------
ALTER TABLE `migrations` AUTO_INCREMENT=9;

-- ----------------------------
-- Auto increment value for users
-- ----------------------------
ALTER TABLE `users` AUTO_INCREMENT=1;
