<?php
namespace bootstrap\cache;


class Request
{
    private static $requestArr = [];

    public function __construct()
    {
        $request = json_decode(file_get_contents('php://input'), true);
        self::$requestArr = $request;

    }

    public static function get($key = null)
    {
        if ($key == null) {
            return self::$requestArr;
        } else {
            return self::$requestArr[$key];
        }
    }

}