<?php
/**
 * Created by Docebo Spa.
 *
 * @link https://www.docebo.com/
 * @copyright Copyright (c) 2016 Docebo Spa
 */


require_once('./vendor/autoload.php');


header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');


require_once(__DIR__ . "/config_helper.php");
require_once(__DIR__ . "/env.php");
require_once(__DIR__ . "/App/common/helper_methods.php");

require_once 'route.php';


try {
    config_helper::loadConfig();
    $route = new Route();
    new \bootstrap\cache\Request();

} catch (Exception $e) {
    dump($e->getMessage());
    die;
}

//ANIMALS
$route->add('/home_page', function () {
    $homeController = new \App\controllers\HomePageController();
    $homeController->index();

});

$route->add('animal/.+', function ($kind) {
    $animalController = new \App\Controllers\AnimalsController();
    $animalController->index($kind);

});

$route->add('/animal_view/.+', function ($id) {
    $animalController = new \App\Controllers\AnimalsController();
    $animalController->view($id);

});


$route->add('/add_edit/animal', function () {
    $animalController = new \App\Controllers\AnimalsController();
    $animalController->addEditAnimal();

});

$route->add('/animal_delete/.+', function ($animalId) {
    $animalController = new \App\Controllers\AnimalsController();
    $animalController->deleteAnimal($animalId);
});


//KINDS
$route->add('/getKinds', function () {
    $animalController = new \App\Controllers\AnimalsController();
    $animalController->getKinds();

});


$route->add('/kind_edit_create', function () {
    $animalController = new \App\Controllers\AnimalsController();
    $animalController->addEditKind();
});

$route->add('/kind_delete/.+', function ($kindId) {
    $animalController = new \App\Controllers\AnimalsController();
    $animalController->deleteKind($kindId);
});


$route->submit();




